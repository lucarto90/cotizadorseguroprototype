// cotizador constructor para seguro

class Seguro {
    constructor (marca, anio, tipo) {
        this.marca = marca;
        this.anio = anio;
        this.tipo = tipo;
    }

    cotizarSeguro () {
        /*
        * 1 = Americano 1.15
        * 2 = Asiatico 1.05
        * 3 = Europeo 1.35
        */
        let cantidad;
        const base = 2000;
        
        switch (this.marca) {
            case '1':
                cantidad = base * 1.15;
                break;
            case '2':
                cantidad = base * 1.05;
                break;
            case '3': 
                cantidad = base * 1.35;
                break;
        }
    
        // leer el año
        const diferencia = new Date().getFullYear() - this.anio;
    
        // cada año de diferencia se reduce el 3% del valor del seguro
        cantidad -= ((diferencia * 3) * cantidad) / 100;
    
        /*
        * si el seguro es basico se multiplica por 30% mas
        * si el seguro es completo se multiplica por 50% mas
        */
        if (this.tipo === 'basico') {
            cantidad *= 1.30;
        } else {
            cantidad *= 1.50;
        }
    
        return cantidad;
    }
}


// todo lo que se muestra
class Interfaz {

    // mensaje que se imprime en el HTML
    mostrarMensaje (mensaje, tipo) {
        const div = document.createElement('div');
    
        if (tipo === 'error') {
            div.classList.add('mensaje','error');
        } else {
            div.classList.add('mensaje','correcto');
        }
    
        div.innerHTML = `${mensaje}`;
    
        formulario.insertBefore(div, document.querySelector('.form-group'));
    
        setTimeout(() => {
            document.querySelector('.mensaje').remove();
        }, 3000);
    }

    // imprimir el resultado de la cotizacion
    mostrarResultado (seguro, total) {
        const resultado = document.getElementById('resultado');
        let marca;
    
        switch (seguro.marca) {
            case '1':
                marca = 'Americano';
                break;
            case '2':
                marca = 'Asiatico';
                break;
            case '3': 
                marca = 'Europeo';
                break;
        }
    
        // crear un div
        const div = document.createElement('div');
        div.innerHTML = `
            <p class='header'>Tu Resumen:</p>
            <p>Marca: ${marca} </p>
            <p>Año: ${seguro.anio} </p>
            <p>Tipo: ${seguro.tipo} </p><br/>
            <p><strong>Total: $ ${total}</strong></p> 
        `;
    
        const spinner = document.querySelector('#cargando img');
        spinner.style.display = 'block';
    
        setTimeout(() => {
            spinner.style.display = 'none'
            resultado.appendChild(div);
        }, 3000);
    }
}


// Event listeners
const formulario = document.getElementById('cotizar-seguro');

formulario.addEventListener('submit', (e) => {
    e.preventDefault();

    // leer la marca seleccionada del select
    const marca = document.getElementById('marca');
    const marcaSeleccionada = marca.options[marca.selectedIndex].value;

    // leer el año seleccionado del select
    const anio = document.getElementById('anio');
    const anioSeleccionado = anio.options[anio.selectedIndex].value;

    // leer el valor del radioButtom
    const tipo = document.querySelector('input[name="tipo"]:checked').value;
    
    // crear instancia de interfaz
    const interfaz = new Interfaz();

    // revisamos que los campos no esten vacios
    if (marcaSeleccionada === '' || anioSeleccionado === '' || tipo === '') {
        // interfaz imprimiendo un error
        interfaz.mostrarMensaje('faltan datos, revisar el formulario', 'error');
    } else {
        // limpiar los resultados previamente consultados
        const resultados = document.querySelector('#resultado div');
        if (resultados != null) {
            resultados.remove();
        }

        // instanciar seguro y mostrar interfaz
        const seguro = new Seguro(marcaSeleccionada, anioSeleccionado, tipo);
        // cotizar el seguero
        const cantidad = seguro.cotizarSeguro(seguro);
        // mostrar el resultado
        interfaz.mostrarResultado(seguro, cantidad);
        interfaz.mostrarMensaje('Cotizando...', 'exito');
    }
});

// llenamos el select con los ultimos años
const max = new Date().getFullYear(),
    min = max - 20;

const selectAnios = document.getElementById('anio');

for (let i = max; i > min; i--) {
    let option = document.createElement('option');
    option.value = i;
    option.innerHTML = i;
    selectAnios.appendChild(option);
}